package com.hunt.things.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import com.mongodb.lang.NonNull;

@Document
public class Thing {
	@Id
	private String id;
	
	private String name;
	private String value;
	
	@NonNull
	private String ownerId;
	
	public String getOwnerId() {
		return this.ownerId;
	}
	
	public void setOwnerId( String ownerId ) {
		this.ownerId = ownerId;
	}
	
	
	private Thing( Builder b ) {
		this.id = b.id;
		this.name = b.name;
		this.ownerId = b.ownerId;
		this.value = b.value;
	}
	
	public Thing() {}
	
	public String getId() {
		return id;
	}
	
	public void setId(String id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}



	public void setName(String name) {
		this.name = name;
	}



	public String getValue() {
		return value;
	}



	public void setValue(String value) {
		this.value = value;
	}



	public static class Builder {
		private String name;
		private String value;
		private String id;
		private String ownerId;
		
		
		public Builder ownerId( String ownerId ) {
			this.ownerId = ownerId;
			return this;
		}
				
		public Builder id( String id ) {
			this.id = id;
			return this;
		}
		
		public Builder name( String name ) {
			this.name = name;
			return this;
		}
		
		public Builder value( String value ) {
			this.value = value;
			return this;
		}
		
		public Thing build() {
			return new Thing(this);
		}
	}
}
