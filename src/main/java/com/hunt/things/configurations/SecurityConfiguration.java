package com.hunt.things.configurations;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;

import com.hunt.things.services.ThingsUserDetailsService;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {
	@Bean
	public PasswordEncoder getPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}
	
	@Override
	public void configure( HttpSecurity http ) throws Exception {
		http
			.authorizeRequests()
				.antMatchers("/customlogin").permitAll()
				.anyRequest().authenticated()
			.and()
				.formLogin()
				.loginProcessingUrl("/customlogin")
				.defaultSuccessUrl("/api/users")
			.and()
			.logout()
				.permitAll();	
			
	}
	
	@Bean
	@Override
	public UserDetailsService userDetailsService() {
		return new ThingsUserDetailsService();
	}
}
