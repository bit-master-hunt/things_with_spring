package com.hunt.things.services;

import java.util.Arrays;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.hunt.things.models.User;

public class ThingsUserDetailsService implements UserDetailsService {
	@Autowired
	private UserService userService;

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		User user = userService.findByEmail(username);

		if( user == null ) throw new UsernameNotFoundException(username);
		
		
		UserDetails details = new UserDetails() {			
			@Override
			public Collection<? extends GrantedAuthority> getAuthorities() {
				return Arrays.asList(
						new SimpleGrantedAuthority("user"),
						new SimpleGrantedAuthority("admin")
				);
			}

			@Override
			public String getPassword() {
				return user.getPassword();
			}

			@Override
			public String getUsername() {
				return user.getEmail();
			}

			@Override
			public boolean isAccountNonExpired() {
				return true;
			}

			@Override
			public boolean isAccountNonLocked() {
				return true;
			}

			@Override
			public boolean isCredentialsNonExpired() {
				return true;
			}

			@Override
			public boolean isEnabled() {
				return true;
			}			
		};
		
		System.out.println(user.getEmail() + ":" + user.getPassword() );
		return details;
	}

}
