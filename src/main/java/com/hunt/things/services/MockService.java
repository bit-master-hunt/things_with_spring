package com.hunt.things.services;

import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.hunt.things.models.Thing;
import com.hunt.things.models.User;
import com.hunt.things.repositories.ThingRepository;
import com.hunt.things.repositories.UserRepository;

@Service
public class MockService {
	@Autowired
	private ThingRepository thingRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private PasswordEncoder encoder;

	@PostConstruct
	public void create() {
		thingRepository.deleteAll();
		userRepository.deleteAll();
						
		User user1 = new User.Builder()
				.email("jimc@ebox.edu")
				.username("Kenny Hunt")
				.password( encoder.encode("123" ) )
				.build();
		
		User user2 = new User.Builder()
				.email("jimc@google.com")
				.username("Jim Bruno")
				.password( encoder.encode("123" ) )
				.build();
		
		user1 = userRepository.save(user1);
		user2 = userRepository.save(user2);
	
		
		List<User> users = Arrays.asList(user1, user2);
		
		users.forEach( user -> {
			for( int i = 0; i < 10; i++ ) {
				Thing thing = new Thing.Builder()
						.name("a")
						.value("b")
						.ownerId( user.getId() )
						.build();
				
				thingRepository.save( thing );
			}
		});
		
	}
}
