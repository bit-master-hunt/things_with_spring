package com.hunt.things.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hunt.things.models.Thing;
import com.hunt.things.repositories.ThingRepository;

@Service
public class ThingService {
	@Autowired
	private ThingRepository thingRepository;

	public List<Thing> getThings(String uid) {
		return thingRepository.findByOwnerId(uid);
	}

	public Thing createThing(Thing thing) {
		return thingRepository.save(thing);
	}

}
