package com.hunt.things.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hunt.things.models.User;
import com.hunt.things.repositories.UserRepository;

@Service
public class UserService {
	@Autowired
	private UserRepository userRepository;

	public List<User> getUsers() {
		return userRepository.findAll();
	}

	public User getUser(String uid) {
		return userRepository.findById( uid ).orElse(null);
	}

	public User createUser(User user) {
		return userRepository.save( user );
	}

	
	public User findByUsername( String username ) {
		return userRepository.findByUsername( username );
	}

	public User findByEmail(String email) {
		return userRepository.findByEmail(email);
	}

}
