package com.hunt.things.repositories;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.hunt.things.models.User;


public interface UserRepository extends MongoRepository<User, String> {
	User findByUsername(String username);
	User findByEmail(String email);
}
