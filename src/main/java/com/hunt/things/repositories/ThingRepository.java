package com.hunt.things.repositories;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.hunt.things.models.Thing;

public interface ThingRepository extends MongoRepository<Thing, String> {

	List<Thing> findByOwnerId(String uid);

}
