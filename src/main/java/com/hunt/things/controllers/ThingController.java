package com.hunt.things.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hunt.things.models.Thing;
import com.hunt.things.services.ThingService;

@RestController
@RequestMapping("/api/users/{uid}/things")
public class ThingController {
	@Autowired
	private ThingService thingService;
	
	@RequestMapping(value="", method=RequestMethod.GET)
	public List<Thing> getThings( @PathVariable String uid ) {
		return thingService.getThings( uid );
	}
	
}
