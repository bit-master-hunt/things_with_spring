package com.hunt.things.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.hunt.things.models.User;
import com.hunt.things.services.UserService;

@RestController
@RequestMapping("/api/users")
public class UserController {
	@Autowired
	private UserService userService;
	
	
	@RequestMapping( value="", method=RequestMethod.GET)
	public List<User> getUsers( ) {
		return userService.getUsers();
	}
	
	@RequestMapping( value="/{uid}", method=RequestMethod.GET )
	public User getUser( @PathVariable String uid ) {
		return userService.getUser( uid );
	}
	
	@RequestMapping( value="", method=RequestMethod.POST)
	public User createUser( @RequestBody User user ) {
		return userService.createUser( user );
	}
}
