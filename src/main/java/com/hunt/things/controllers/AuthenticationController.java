package com.hunt.things.controllers;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class AuthenticationController {
	
	@RequestMapping(value="/customlogin", method=RequestMethod.GET)
	public String getLoginForm( ) {
		return "<div></div>";
	}
}
