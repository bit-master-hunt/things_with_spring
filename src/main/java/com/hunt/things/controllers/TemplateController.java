package com.hunt.things.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.hunt.things.models.User;
import com.hunt.things.services.UserService;

@Controller
public class TemplateController {
	@Autowired
	UserService userService;
	
	
	// Returns the name of a template (an HTML document)
	@RequestMapping(value="/users", method=RequestMethod.GET)
	public String getAllUsers(Model model) {
		List<User> users = userService.getUsers();
		model.addAttribute("users", users);
		return "users";
	}
}
